# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160315224601) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "attendances", force: :cascade do |t|
    t.string   "rsvp"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "event_id",   null: false
    t.integer  "user_id",    null: false
    t.integer  "invite_id",  null: false
  end

  add_index "attendances", ["event_id"], name: "index_attendances_on_event_id", using: :btree
  add_index "attendances", ["invite_id"], name: "index_attendances_on_invite_id", using: :btree
  add_index "attendances", ["user_id"], name: "index_attendances_on_user_id", using: :btree

  create_table "event_feedbacks", force: :cascade do |t|
    t.decimal  "rating",        precision: 4, scale: 2
    t.text     "comment"
    t.integer  "attendance_id",                         null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "event_feedbacks", ["attendance_id"], name: "index_event_feedbacks_on_attendance_id", using: :btree

  create_table "events", force: :cascade do |t|
    t.string   "title",                       null: false
    t.text     "description"
    t.boolean  "is_public",   default: false
    t.datetime "start_time"
    t.datetime "end_time"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "user_id"
    t.integer  "venue_id"
  end

  add_index "events", ["user_id"], name: "index_events_on_user_id", using: :btree
  add_index "events", ["venue_id"], name: "index_events_on_venue_id", using: :btree

  create_table "invites", force: :cascade do |t|
    t.string   "email",      null: false
    t.string   "token",      null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "event_id",   null: false
    t.datetime "sent_at"
  end

  add_index "invites", ["event_id"], name: "index_invites_on_event_id", using: :btree
  add_index "invites", ["token"], name: "index_invites_on_token", using: :btree

  create_table "notifications", force: :cascade do |t|
    t.string   "message"
    t.string   "trigger"
    t.integer  "user_id",                        null: false
    t.string   "alertable_type"
    t.integer  "alertable_id"
    t.boolean  "is_viewed",      default: false
    t.boolean  "is_read",        default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
  end

  add_index "notifications", ["alertable_type", "alertable_id"], name: "index_notifications_on_alertable_type_and_alertable_id", using: :btree
  add_index "notifications", ["user_id"], name: "index_notifications_on_user_id", using: :btree

  create_table "photos", force: :cascade do |t|
    t.string   "image_file_name"
    t.string   "image_content_type"
    t.integer  "image_file_size"
    t.boolean  "is_profile",         default: false
    t.integer  "height"
    t.integer  "width"
    t.string   "pictureable_type"
    t.integer  "pictureable_id"
    t.datetime "created_at",                         null: false
    t.datetime "updated_at",                         null: false
    t.integer  "owner_id",                           null: false
  end

  add_index "photos", ["owner_id"], name: "index_photos_on_owner_id", using: :btree
  add_index "photos", ["pictureable_type", "pictureable_id"], name: "index_photos_on_pictureable_type_and_pictureable_id", using: :btree

  create_table "posts", force: :cascade do |t|
    t.text     "content"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "user_id",    null: false
    t.integer  "event_id",   null: false
    t.integer  "photo_id"
  end

  add_index "posts", ["event_id"], name: "index_posts_on_event_id", using: :btree
  add_index "posts", ["photo_id"], name: "index_posts_on_photo_id", using: :btree
  add_index "posts", ["user_id"], name: "index_posts_on_user_id", using: :btree

  create_table "profiles", force: :cascade do |t|
    t.boolean  "is_complete",       default: false
    t.string   "gender"
    t.integer  "age"
    t.string   "ethnicity"
    t.string   "country_of_origin"
    t.string   "education"
    t.string   "purpose"
    t.string   "astrology"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.integer  "user_id",                           null: false
  end

  add_index "profiles", ["user_id"], name: "index_profiles_on_user_id", using: :btree

  create_table "tags", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  add_index "tags", ["name"], name: "index_tags_on_name", using: :btree

  create_table "users", force: :cascade do |t|
    t.string   "first_name"
    t.string   "last_name"
    t.string   "email",           null: false
    t.string   "password_digest"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.string   "username"
    t.string   "remember_digest"
  end

  add_index "users", ["username"], name: "index_users_on_username", unique: true, using: :btree

  create_table "venue_feedbacks", force: :cascade do |t|
    t.decimal  "rating",        precision: 4, scale: 2
    t.text     "comment"
    t.integer  "attendance_id",                         null: false
    t.datetime "created_at",                            null: false
    t.datetime "updated_at",                            null: false
  end

  add_index "venue_feedbacks", ["attendance_id"], name: "index_venue_feedbacks_on_attendance_id", using: :btree

  create_table "venue_tags", force: :cascade do |t|
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.integer  "venue_id",   null: false
    t.integer  "tag_id",     null: false
  end

  add_index "venue_tags", ["tag_id"], name: "index_venue_tags_on_tag_id", using: :btree
  add_index "venue_tags", ["venue_id"], name: "index_venue_tags_on_venue_id", using: :btree

  create_table "venues", force: :cascade do |t|
    t.string   "name"
    t.text     "description"
    t.string   "website"
    t.float    "latitude"
    t.float    "longitude"
    t.integer  "capacity"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "user_id"
    t.string   "city"
    t.string   "street"
    t.string   "suite_no"
  end

  add_index "venues", ["user_id"], name: "index_venues_on_user_id", using: :btree

  add_foreign_key "attendances", "events", on_delete: :cascade
  add_foreign_key "attendances", "invites", on_delete: :cascade
  add_foreign_key "attendances", "users", on_delete: :cascade
  add_foreign_key "event_feedbacks", "attendances"
  add_foreign_key "events", "users"
  add_foreign_key "events", "venues"
  add_foreign_key "invites", "events", on_delete: :cascade
  add_foreign_key "notifications", "users", on_delete: :cascade
  add_foreign_key "photos", "users", column: "owner_id", on_delete: :cascade
  add_foreign_key "posts", "events", on_delete: :cascade
  add_foreign_key "posts", "photos"
  add_foreign_key "posts", "users", on_delete: :cascade
  add_foreign_key "profiles", "users", on_delete: :cascade
  add_foreign_key "venue_feedbacks", "attendances", on_delete: :cascade
  add_foreign_key "venue_tags", "tags", on_delete: :cascade
  add_foreign_key "venue_tags", "venues", on_delete: :cascade
  add_foreign_key "venues", "users"
end
