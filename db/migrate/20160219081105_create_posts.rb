class CreatePosts < ActiveRecord::Migration[5.0]
  def change
    create_table :posts do |t|
    	t.text :content
      t.timestamps null: false
    end
    add_reference :posts, :user, index: true, null: false, foreign_key: { on_delete: :cascade }
    add_reference :posts, :event, index: true, null: false, foreign_key: { on_delete: :cascade }
  end
end
