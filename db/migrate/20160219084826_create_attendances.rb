class CreateAttendances < ActiveRecord::Migration[5.0]
  def change
    create_table :attendances do |t|
    	t.string :rsvp
      t.timestamps null: false
    end
    add_reference :attendances, :event, index: true, null: false, foreign_key: { on_delete: :cascade }
    add_reference :attendances, :user, index: true, null: false, foreign_key: { on_delete: :cascade }
    add_reference :attendances, :invite, index: true, null: false, unique: true, foreign_key: { on_delete: :cascade }
  end
end
