class CreateVenueTags < ActiveRecord::Migration[5.0]
  def change
    create_table :venue_tags do |t|
      t.timestamps null: false
    end
    add_reference :venue_tags, :venue, index: true, null: false, foreign_key: { on_delete: :cascade }
    add_reference :venue_tags, :tag, index: true, null: false, foreign_key: { on_delete: :cascade }
  end
end
