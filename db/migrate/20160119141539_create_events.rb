class CreateEvents < ActiveRecord::Migration[5.0]
  def change
    create_table :events do |t|
      t.string :title, null: false
      t.text :description
      t.boolean :is_public, default: false
      t.datetime :start_time
      t.datetime :end_time
      t.timestamps null: false
    end
  end
end
