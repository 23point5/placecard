class CreateVenues < ActiveRecord::Migration[5.0]
  def change
    create_table :venues do |t|
      t.string :name
      t.text :description
      t.string :website
      t.string :address
      t.float :latitude
      t.float :longitude
      t.integer :capacity
      t.timestamps null: false
    end
  end
end
