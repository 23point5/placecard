class CreateEventFeedbacks < ActiveRecord::Migration[5.0]
  def change
    create_table :event_feedbacks do |t|
      t.decimal :rating, precision: 4, scale: 2
      t.text :comment
      t.references :attendance, index: true, foreign_key: true, null: false, on_delete: :cascade
      t.timestamps null: false
    end
  end
end
