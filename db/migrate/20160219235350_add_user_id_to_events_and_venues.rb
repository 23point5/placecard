class AddUserIdToEventsAndVenues < ActiveRecord::Migration[5.0]
  def change
  	add_reference :events, :user, foreign_key: true, index: true
  	add_reference :events, :venue, foreign_key: true, index: true
  	add_reference :venues, :user, foreign_key: true, index: true
  end
end
