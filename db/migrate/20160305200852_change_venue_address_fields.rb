class ChangeVenueAddressFields < ActiveRecord::Migration[5.0]
  def change
  	remove_column :venues, :address, :string
  	add_column :venues, :city, :string
  	add_column :venues, :street, :string
  	add_column :venues, :suite_no, :string
  end
end
