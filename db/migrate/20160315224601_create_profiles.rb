class CreateProfiles < ActiveRecord::Migration[5.0]
  def change
    create_table :profiles do |t|
    	t.boolean :is_complete, default: false

    	t.string :gender
    	t.integer :age
    	t.string :ethnicity
      t.string :country_of_origin
    	
    	t.string :education
    	t.string :purpose
    	t.string :astrology

      t.timestamps null: false
    end
    add_reference :profiles, :user, index: true, null: false, foreign_key: { on_delete: :cascade }
  end
end
