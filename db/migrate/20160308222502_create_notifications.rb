class CreateNotifications < ActiveRecord::Migration[5.0]
  def change
    create_table :notifications do |t|
      t.string :message
      t.string :trigger
      t.references :user, index: true, null: false, foreign_key: { on_delete: :cascade }
      t.references :alertable, polymorphic: true, index: true
      t.boolean :is_viewed, default: false
      t.boolean :is_read, default: false
      t.timestamps null: false
    end
  end
end
