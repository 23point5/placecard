class AddSentAtToInvites < ActiveRecord::Migration[5.0]
  def change
  	add_column :invites, :sent_at, :datetime
  end
end
