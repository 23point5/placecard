class CreateInvites < ActiveRecord::Migration[5.0]
  def change
    create_table :invites do |t|
      t.string :email, null: false
      t.string :token, null: false, unique: true
      t.timestamps null: false
    end
    add_index :invites, :token
    add_reference :invites, :event, index: true, null: false, foreign_key: { on_delete: :cascade }
  end
end
