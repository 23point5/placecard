class CreatePhotos < ActiveRecord::Migration[5.0]
  def change
    create_table :photos do |t|
        t.string :image_file_name
        t.string :image_content_type
        t.integer :image_file_size
        t.boolean :is_profile, default: false
        t.integer :height
        t.integer :width
        t.references :pictureable, polymorphic: true, index: true
        t.timestamps null: false
    end
    add_reference :posts, :photo, foreign_key: true
    add_reference :photos, :owner, references: :users, index: true, null: false
    add_foreign_key :photos, :users, column: :owner_id, on_delete: :cascade
  end
end
