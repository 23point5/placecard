# /*====================================
# =            Common Seeds            =
# ====================================*/

# /*----------  Users  ----------*/

placecard = User.find_or_create_by(username: "placecard") do |p|
  p.email = "placecard@gmail.com"
  p.first_name = "Place"
  p.last_name = "Card"
  p.password = "PlaceCard_p!"
end

saige = User.find_or_create_by(username: "saige") do |s|
  s.email = "saigemcvea@gmail.com"
  s.first_name = "Saige"
  s.last_name = "McVea"
  s.password = "PlaceCard_s!"
end

gabi = User.find_or_create_by(username: "gabi") do |g|
  g.email = "lastgabs@gmail.com"
  g.first_name = "Gabriela"
  g.last_name = "Stefanini"
  g.password = "PlaceCard_g!"
end

kathryn = User.find_or_create_by(username: "kathryn") do |k|
  k.email = "kathryn.taylor@mail.mcgill.ca"
  k.first_name = "Kathryn"
  k.last_name = "Taylor"
  k.password = "PlaceCard_k!"
end

zili = User.find_or_create_by(username: "zili") do |z|
	z.email = "zhangzili0501@gmail.com"
	z.first_name = "Zili"
	z.last_name = "Zhang"
	z.password = "PlaceCard_z!"
end

# /*----------  Profiles  ----------*/

p_profile = Profile.find_by_user_id(placecard.id)
p_profile.update_attributes({
  gender: 'other',
  age: 0,
  country_of_origin: 'Canada',
  education: 'university',
  purpose: 'all',
  astrology: 'aries'
})
s_profile = Profile.find_by_user_id(saige.id)
s_profile.update_attributes({
  gender: 'female',
  age: 23,
  ethnicity: 'caucasian',
  country_of_origin: 'Canada',
  education: 'university',
  purpose: 'event_participant',
  astrology: 'capricorn'
})
g_profile = Profile.find_by_user_id(gabi.id)
g_profile.update_attributes({
  gender: 'female',
  age: 27,
  ethnicity: 'mixed',
  country_of_origin: 'Brazil',
  education: 'university',
  purpose: 'event_organizer',
  astrology: 'scorpio'
})
k_profile = Profile.find_by_user_id(kathryn.id)
k_profile.update_attributes({
  gender: 'female',
  age: 20,
  ethnicity: 'caucasian',
  country_of_origin: 'United States',
  education: 'university',
  purpose: 'venue_owner',
  astrology: 'virgo'
})
z_profile = Profile.find_by_user_id(zili.id)
z_profile.update_attributes({
  gender: 'female',
  age: 26,
  ethnicity: 'asian',
  country_of_origin: 'China',
  education: 'university',
  purpose: 'venue_owner',
  astrology: 'libra'
})

# /*----------  Venues  ----------*/

VCC = Venue.find_or_create_by(name: "Vancouver Convention Centre") do |vcc|
  vcc.description = "One of Canada's largest convention centres, it was awarded the International Association of Congress Centres (AIPC) 'Apex Award' for the 'World's Best Congress Centre'."
  vcc.website = "http://www.vancouverconventioncentre.com"
  vcc.capacity = 12_000
  vcc.city = "Vancouver, BC"
  vcc.street = "1055 Canada Place"
  vcc.user_id = placecard.id
end
BH = Venue.find_or_create_by(name: "John M.S. Lecky UBC Boathouse") do |bh|
  bh.description = "The hall and balcony capture the natural beauty of the river together with the stunning panoramic views of the North Shore Mountains. It is an unique and ideal setting for weddings, corporate meetings, receptions or parties."
  bh.website = "http://www.ubcboathouse.com/index.html"
  bh.capacity = 190
  bh.city = "Vancouver, BC"
  bh.street = "7277 River Road"
  bh.user_id = saige.id
end
SW = Venue.find_or_create_by(name: "Science World") do |sw|
  sw.description = "Innovative events demand creative settings and we offer you limitless options for your special meeting or gathering. We can accommodate groups from 20 to 1,200—from fun evenings for employees and clients to sit-down formal dinners, buffet dinners and cocktail receptions. Outdoor spaces, hands-on galleries, theatres that can seat up to 400…let your imagination run wild!"
  sw.website = "http://www.scienceworld.bc.ca"
  sw.capacity = 1_200
  sw.city = "Vancouver, BC"
  sw.street = "1455 Quebec Street"
  sw.user_id = saige.id
end
HM = Venue.find_or_create_by(name: "Hycroft Mansion") do |hm|
  hm.description = "The house is regularly used for celebrations, weddings, retirements, fundraising, lectures, meetings, conferences, book launches, business events, concerts as well as movies and photo shoots."
  hm.website = "http://www.uwcvancouver.ca"
  hm.capacity = 250
  hm.city = "Vancouver, BC"
  hm.street = "1489 McRae Avenue"
  hm.user_id = gabi.id
end

HLC = Venue.find_or_create_by(name: "Hotel Le Crystal") do |hlc|
  hlc.description = "Experience the very best of Montreal during your stay at our boutique Hotel Le Crystal in the heart of downtown Montreal's best dining, attractions and more."
  hlc.website = "http://www.hotellecrystal.com"
  hlc.capacity = 350
  hlc.city = "Montreal, QC"
  hlc.street = "1489 McRae Avenue"
  hlc.user_id = gabi.id
end
MP = Venue.find_or_create_by(name: "Montreal Planetarium") do |mp|
  mp.description = "The Rio Tinto Alcan Planetarium is the successor to the Montreal planetarium, and is located in the Espace pour la Vie, near the Olympic stadium and the Biodome. The new installation has two separate theatres as well as exhibits on space and astronomy."
  mp.website = "http://espacepourlavie.ca/en/planetarium"
  mp.capacity = 1_000
  mp.city = "Montreal, QC"
  mp.street = "1000 Rue Saint-Jacques"
  mp.user_id = zili.id
end
BC = Venue.find_or_create_by(name: "Bell Centre") do |mp|
  mp.description = "Centre Bell is a sports and entertainment complex. It opened on March 16, 1996 after nearly three years under construction. It is best known as the home of the National Hockey League's Montreal Canadiens ice hockey team."
  mp.website = "http://www.centrebell.ca"
  mp.capacity = 21_273
  mp.city = "Montreal, QC"
  mp.street = "1909, Avenue des Canadiens-de-Montreal"
  mp.user_id = zili.id
end

BCC = Venue.find_or_create_by(name: "Burlington Convention Centre") do |bcc|
  bcc.description = "Romance drifts in the air as you and your guests enter the Reception Hall Grand Foyer to the enchanting sounds of the grand piano. Modern/contemporary decor and soaring ceilings studded with exquisite crystal chandeliers set the stage for your special occasion."
  bcc.website = "http://burlingtonconventioncentre.ca"
  bcc.capacity = 1_500
  bcc.city = "Toronto, ON"
  bcc.street = "1120 Burloak Drive"
  bcc.user_id = kathryn.id
end
AG = Venue.find_or_create_by(name: "Arta Gallery") do |ag|
  ag.description = "Host your next event in Arta Gallery's modern and intriguing venue. This unique space is perfect for any style event including weddings, corporate functions, social events and fundraisers. Located in the Historic Distillery District in downtown Toronto, this ambient and remarkably flexible space is sure to create a memorable event for all of your guests."
  ag.website = "http://www.artagallery.ca/exhibition/"
  ag.capacity = 220
  ag.city = "Toronto, ON"
  ag.street = "14 Distillery Lane"
  ag.user_id = kathryn.id
end
CN = Venue.find_or_create_by(name: "CN Tower") do |cn|
  cn.description = "The ultimate event setting! Multiple venues perfect for formal dining, receptions, meetings, launches, parties & more. Impress your guests with breathtaking views, impeccable service, and award winning cuisine at Canada's Wonder of the World."
  cn.website = "http://www.cntower.ca/intro.html"
  cn.capacity = 2_000
  cn.city = "Toronto, ON"
  cn.street = "301 Front Street West"
  cn.user_id = placecard.id
end

# /*----------  Tags  ----------*/

Tag::VENUE_TYPES.each do |type|
  Tag.find_or_create_by(name: type)
end

Tag::VENUE_FEATURES.each do |feature|
  Tag.find_or_create_by(name: feature)
end
