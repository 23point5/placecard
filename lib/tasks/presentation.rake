namespace :db do
  desc "Add profile data for presentation"
  task seed_presentation: :environment do

    BCC = Venue.find_or_create_by(name: "Burlington Convention Centre")

    # /*----------  Events  ----------*/
    UB = Event.find_or_create_by(title: "Ubisoft Xmas Party") do |ub|
      ub.description = "The best xmas party the world of games has ever seen!"
      ub.is_public = false
      ub.start_time = DateTime.new(2015,12,23,18)
      ub.end_time = DateTime.new(2015,12,24,01)
      ub.created_at = DateTime.new(2015,11,01,12)
      ub.user_id = 3
      ub.venue_id = BCC.id
    end

    HB = Event.find_or_create_by(title: "Bailey & Huni Wedding") do |hb|
      hb.description = "Until everyone is driven to insanity."
      hb.is_public = true
      hb.start_time = DateTime.new(2016,01,05,18)
      hb.end_time = DateTime.new(2016,01,07,11)
      hb.created_at = DateTime.new(2015,12,10,12)
      hb.user_id = 4
      hb.venue_id = BCC.id
    end

    TR = Event.find_or_create_by(title: "Trump Canada Wall Financing") do |tr|
      tr.description = "And we're gonna make you pay for it too!"
      tr.is_public = true
      tr.start_time = DateTime.new(2016,02,04,15)
      tr.end_time = DateTime.new(2016,02,04,20)
      tr.created_at = DateTime.new(2016,01,03,10)
      tr.user_id = 5
      tr.venue_id = BCC.id
    end

    # /*----------  Attendance  ----------*/
    un = ['darthvader', 'laracroft', 'mario', 'cinderella', 'barbie', 'sonic', 'lebowski', 'galactica', 'ariel', 'toad']

    (6..15).each do |n|
      fname  = Faker::Name.first_name
      lname  = Faker::Name.last_name
      uname = un[n-6]
      email = "#{fname}-#{n}@coolusers.org"
      password = "password123"
      User.create!(username: uname, 
                   first_name:  fname,
                   last_name: lname,
                   email: email,
                   password: password)
      token = SecureRandom.urlsafe_base64(10, false)
      Invite.create!(email: email, token: token, event_id: HB.id)
    end

    invite_first = Invite.last.id-9
    user_first_id = User.last.id-9

    (user_first_id..user_first_id+9).each do |n|
      Attendance.create!(rsvp: 'attending', event_id: HB.id, user_id: n, invite_id: invite_first+(n-6))
    end

    gd = ['female', 'male', 'other']
    ethn = ['caucasian', 'asian', 'other', 'hispanic', 'black', 'aboriginal']
    co = ['Canada', 'United States', 'Mexico', 'China']
    zodiac =['aries', 'leo', 'sagittarius', 'taurus', 'virgo', 'capricorn', 'gemini', 'libra', 'aquarius', 'cancer', 'scorpio', 'pisces']
    edu = ['high_school', 'post_secondary', 'technical']
    prng = Random.new

    (6..15).each do |s|
      s_profile = Profile.find_by_user_id(s)
      s_profile.update_attributes({
        gender: gd[prng.rand(0..2)],
        age: prng.rand(17..40),
        ethnicity: ethn[prng.rand(0..5)],
        country_of_origin: co[prng.rand(0..3)],
        education: co[prng.rand(0..2)],
        purpose: 'event_participant',
        astrology: zodiac[prng.rand(0..11)]
      })
    end

  end
end
