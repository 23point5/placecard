# This task automatically populates the db with fake data for testing
# use `rake db:faker` to run

namespace :db do
	desc "Populate database with fake data"
	task faker: :environment do
		
		Event.delete_all
		Venue.delete_all
		# User.delete_all

		# 10.times { FactoryGirl.create(:user) }
		10.times { FactoryGirl.create(:venue) }
		10.times { FactoryGirl.create(:event) }
		100.times { FactoryGirl.create(:invite) }
		50.times { FactoryGirl.create(:attendance) }

	end
end
