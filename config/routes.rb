Rails.application.routes.draw do

  mount ActionCable.server => '/cable'

  get '/about', to: 'pages#about', as: 'about'
  get '/join', to: 'users#new', as: 'join'
  post '/join', to: 'users#create', as: 'users'
  
  get '/login', to: 'sessions#new', as: 'login'
  post 'login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy', as: 'logout'

  get '/venues', to: 'venues#public_index', as: 'public_venues' 
  get '/venues/:id', to: 'venues#show', as: 'venue'

  get '/events', to: 'events#public_index', as: 'public_events' 
  resources :events, only: [:show] do
    resources :posts, only: [:create, :update, :destroy]
  end

  resources :users, param: :username, path: '/', except: [:index, :create, :new] do
  	resources :events, except: [:edit, :show] do
      get '/manage/:step', to: 'events#manage', as: 'manage'
      post '/guests', to: 'events#add_guests', as: 'add_guests'
      post '/invite', to: 'events#send_invites', as: 'send_invites'
      delete '/guests/:id', to: 'events#remove_guest', as: 'remove_guest'
      get '/location/:id', to: 'events#preview_venue', as: 'preview_venue'
      post '/location/:id', to: 'events#select_venue', as: 'select_venue'
      delete 'location/:id', to: 'events#remove_venue', as: 'remove_venue'
    end
  	resources :venues, except: [:edit, :show] do
      get '/manage/:step', to: 'venues#manage', as: 'manage'
      post '/photos', to: 'venues#update_photos', as: 'update_photos'
      delete '/photos/:id', to: 'venues#remove_photo', as: 'remove_photo'
      post '/tags', to: 'venues#update_tags', as: 'update_tags'
    end
    resources :photos, only: [:create, :destroy]
    get '/events/:id', to: 'events#stats', as: 'event_stats'
    get '/venues/:id', to: 'venues#stats', as: 'venue_stats'
    get '/tell-me-more', to: 'profiles#edit', as: 'profile'
    post '/tell-me-more', to: 'profiles#update', as: 'update_profile'
    post '/rsvp/:id', to: 'users#rsvp', as: 'rsvp'
  end

  get '/review/:attendance_id', to: 'feedbacks#rate_event_and_venue', as: 'feedback'
  post '/review/:attendance_id', to: 'feedbacks#submit', as: 'submit_feedback'

  root 'pages#index'

end
