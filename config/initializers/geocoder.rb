Geocoder.configure(
  timeout: 15,
  cache_prefix: :geocoder,
  units: :km,
  lookup: :mapbox,
  mapbox: {
    api_key: ENV['MAPBOX_API_KEY']
  },
  ip_lookup: :freegeoip
)
