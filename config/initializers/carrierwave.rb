CarrierWave.configure do |config|
  
  # config.fog_provider = 'fog'
  config.root = Rails.root.join('tmp')
  config.cache_dir = "uploads"

  # For test & development upload files to local `tmp` folder.
  if Rails.env.test? || Rails.env.development?
    config.storage = :file
    # config.enable_processing = false
  else
    config.storage = :fog
    config.fog_directory = ENV['S3_BUCKET_NAME']
    config.fog_credentials = {
	    provider:								'AWS',
	    aws_access_key_id: 			ENV['S3_KEY'],
	    aws_secret_access_key: 	ENV['S3_SECRET']
	  }
  end

end
