# == Schema Information
#
# Table name: venues
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  website     :string
#  latitude    :float
#  longitude   :float
#  capacity    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  city        :string
#  street      :string
#  suite_no    :string
#
# Indexes
#
#  index_venues_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_ad8f6027f0  (user_id => users.id)
#

class Venue < ApplicationRecord
	include Notifiable
	include Filterable

	belongs_to :user
	has_many :events, dependent: :nullify
	has_many :venue_feedbacks, through: :events
	has_many :photos, as: :pictureable, dependent: :destroy
	has_many :venue_tags
	has_many :tags, through: :venue_tags
	has_many :organizers, through: :events, source: :user
	has_many :organizer_profiles, through: :organizers, source: :profile

	accepts_nested_attributes_for :photos, :tags

	validates :user_id, :name, :city, :street, :capacity, presence: true
	validates :address, presence: true

	geocoded_by :address
	after_validation :geocode

	# `categories` is an array of satisfactory categories
	scope :category, -> (categories) { Venue.includes(:tags).where("tags.name": categories) }
	# `tag` is a single parameter (no arrays)
	scope :tagged, -> (tag) { Tag.find_by_name("#{tag}").venues if Tag.find_by_name("#{tag}") }

	def address
    address = ""
    if !suite_no.blank?
    	address += suite_no + "-"
    end
	if !street.blank? && !city.blank?
		address += [street,city].join(", ")
	end
  end

  def default_photo
  	photos.where(is_profile: true).first || photos.first
  end

	def self.search(search)
		if search
			near(search, 100)
			# where("description LIKE ?", "%#{search}%") @TODO fix this or elaborate search
	  else
	    all
	  end
	end

end
