# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  title       :string           not null
#  description :text
#  is_public   :boolean          default(FALSE)
#  start_time  :datetime
#  end_time    :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  venue_id    :integer
#
# Indexes
#
#  index_events_on_user_id   (user_id)
#  index_events_on_venue_id  (venue_id)
#
# Foreign Keys
#
#  fk_rails_0cb5590091  (user_id => users.id)
#  fk_rails_f476266cf4  (venue_id => venues.id)
#

class Event < ApplicationRecord
	include Notifiable

	belongs_to :user
	belongs_to :venue
	has_many :invites
	has_many :attendances
	has_many :posts, dependent: :delete_all
	has_many :venue_feedbacks, through: :attendances
	has_many :event_feedbacks, through: :attendances
	has_many :photos, as: :pictureable, dependent: :delete_all

	has_many :attendees, -> { where "rsvp <> 'not_attending'" }, class_name: "Attendance"
	has_many :guests, through: :attendees, source: :user
	has_many :guest_profiles, through: :guests, source: :profile

	accepts_nested_attributes_for :invites

	validates :title, :description, :user_id, presence: true
	validate :future_start_time, if: "start_time.present?"
	validate :duration_must_be_positive
	validate :future_start_time, on: :update, if: "self.start_time_changed?"

	after_create_commit { 
		if start_time && start_time > Time.now
			FeedbackGeneratorJob.perform_in(feedback_opens_at - Time.now, self) if end_time 
		end
	}
	after_update :alert_guests, :update_feedback_period
	before_destroy { notify_many(guests, "event_canceled", self) }

	def duration
		end_time - start_time
	end

	def feedback_opens_at
		end_time + 12.hours unless end_time.nil?
		# Time.now + 10.seconds ## for debugging use only
	end

	def feedback_closes_at
		feedback_opens_at + 1.week
	end

	def generate_feedback_forms
		attendees.each do |attendee|
			EventFeedback.create(attendance_id: attendee.id)
			VenueFeedback.create(attendance_id: attendee.id)
		end
	end

  def display_image
  	if self.venue && self.venue.photos.any?
  		self.venue.default_photo.image_file_name
  	else
      "placeholder.png"
    end
  end

  def venue_recs(ip)
  	request = Geocoder.search(ip).first
  	# request_city = "Montreal" ## for debugging use only
  	if !request || !request.city || request.city.blank?
  		request_city = "Montreal"
  	end
  	best_venues = Venue.near(request_city)
  	best_venues = best_venues.where("capacity > ?", self.invites.count) if self.invites.any?
  	best_venues.limit(3)
  end

	private

		def duration_must_be_positive
			if (start_time.present? && end_time.present?)
				(errors[:base] = "start time and end time conflict") unless (end_time - start_time > 0)
			end
		end

		def future_start_time
			if start_time.present? && start_time < Time.now
				errors[:start_time] = "has already past"
			end
		end

		def alert_guests
			if self.title_changed?
				notify_many(guests, "new_event_title", self)
			elsif self.description_changed?
				notify_many(guests, "new_event_description", self)
			elsif self.venue_id_changed?
				notify_many(guests, "new_event_location", self)
			elsif (self.start_time_changed? || self.end_time_changed?)
				notify_many(guests, "new_event_time", self)
			end
		end

		def update_feedback_period
			if self.end_time_changed? && !self.end_time.nil? && self.end_time > Time.now
				FeedbackGeneratorJob.perform_in(feedback_opens_at - Time.now, self)
			end
		end
	
end
