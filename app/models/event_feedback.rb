# == Schema Information
#
# Table name: event_feedbacks
#
#  id            :integer          not null, primary key
#  rating        :decimal(4, 2)
#  comment       :text
#  attendance_id :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_event_feedbacks_on_attendance_id  (attendance_id)
#
# Foreign Keys
#
#  fk_rails_87f91bf6f6  (attendance_id => attendances.id)
#

class EventFeedback < ApplicationRecord
	include Notifiable

	belongs_to :attendance
	has_one :event, through: :attendance
	has_one :user, through: :attendance

	validates :attendance_id, presence: true
	validate :must_be_complete, on: :update

	after_update_commit { notify(event.user, "new_event_feedback", self) }

	def complete?
		rating.present? && comment.present?
	end

	private

		def must_be_complete
			unless complete?
				errors.add(:rating, "Both rating and comment are required.")
			end
		end

end
