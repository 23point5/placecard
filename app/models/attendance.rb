# == Schema Information
#
# Table name: attendances
#
#  id         :integer          not null, primary key
#  rsvp       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  event_id   :integer          not null
#  user_id    :integer          not null
#  invite_id  :integer          not null
#
# Indexes
#
#  index_attendances_on_event_id   (event_id)
#  index_attendances_on_invite_id  (invite_id)
#  index_attendances_on_user_id    (user_id)
#
# Foreign Keys
#
#  fk_rails_777eb7170a  (event_id => events.id)
#  fk_rails_77ad02f5c5  (user_id => users.id)
#  fk_rails_d50c017781  (invite_id => invites.id)
#

class Attendance < ApplicationRecord
	include Notifiable

	RSVP = [ 'attending', 'not_attending', 'unsure' ]

	belongs_to :event
	belongs_to :user
	belongs_to :invite
	has_one :venue_feedback, dependent: :destroy
	has_one :event_feedback, dependent: :destroy

	validates :user_id, :event_id, :invite_id, presence: true

	after_update :alert_rsvp

	private

		def alert_rsvp
			if self.rsvp_changed?
				case self.rsvp
				when "attending"
					notify(event.user, :rsvp_attending, self)
				when "not_attending"
					notify(event.user, :rsvp_not_attending, self)
				end
			end
		end

end
