module Filterable
	extend ActiveSupport::Concern

	module ClassMethods

		##
		# This method assumes it is being passed an array of tags
		# where a satisfactory result has each of the tags in the array
		# OR
		# is passed a single parameter that must be satisfied
		##
		def filter(tags)
      results = self.where(nil)
      tags.each do |key, value|
      	if value.kind_of?(Array)
        	value.each do |val|
        		results = results.public_send(key, val) if val.present?
        	end
        else
        	results = results.public_send(key, value) if value.present?
        end
      end
      results
    end

	end
end