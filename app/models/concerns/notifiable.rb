module Notifiable
	extend ActiveSupport::Concern

	def notify(user, trigger, caller)
		Notification.create(user_id: user.id, trigger: trigger, alertable: caller)
	end

	def notify_many(users, trigger, caller)
		users.each do |user|
			Notification.create(user_id: user.id, trigger: trigger, alertable: caller)
		end
	end

end
