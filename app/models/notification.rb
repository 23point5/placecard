# == Schema Information
#
# Table name: notifications
#
#  id             :integer          not null, primary key
#  message        :string
#  trigger        :string
#  user_id        :integer          not null
#  alertable_type :string
#  alertable_id   :integer
#  is_viewed      :boolean          default(FALSE)
#  is_read        :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_notifications_on_alertable_type_and_alertable_id  (alertable_type,alertable_id)
#  index_notifications_on_user_id                          (user_id)
#
# Foreign Keys
#
#  fk_rails_b080fb4855  (user_id => users.id)
#

class Notification < ApplicationRecord

	belongs_to :user
	belongs_to :alertable, polymorphic: true

	validates :user_id, presence: true

	before_create :set_message
	after_create_commit { NotificationRelayJob.perform_later self}

	def view
		update_attributes(is_viewed: true)
	end

	def read
		update_attributes(is_read: true)
	end

	private

    def set_message
    	case trigger
    	when "rsvp_attending"
    		self.message = "#{alertable.user.full_name} is going to your event."
    	when "rsvp_not_attending"
    		self.message = "#{alertable.user.full_name} cannot go to your event."
    	when "new_event_title"
    		self.message = "#{alertable.user.full_name} changed the name of their event to #{alertable.title}."
    	when "new_event_description"
    		self.message = "#{alertable.user.full_name} updated information for #{alertable.title}."
    	when "new_event_location"
    		self.message = "#{alertable.title} has a new venue."
    	when "new_event_time"
    		self.message = "#{alertable.user.full_name} changed the time of #{alertable.title}."
    	when "event_organizer_post"
    		self.message = "#{alertable.user.full_name} posted to #{alertable.event.title}"
    	when "event_canceled"
    		self.message = "#{alertable.user.full_name} canceled #{alertable.title}."
    	when "new_event_feedback"
    		self.message = "New feedback was left for #{alertable.event.title}."
    	when "new_venue_feedback"
    		self.message = "New feedback was left for #{alertable.venue.name}."
    	end
    end

end
