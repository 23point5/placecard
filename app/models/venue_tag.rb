# == Schema Information
#
# Table name: venue_tags
#
#  id         :integer          not null, primary key
#  venue_id   :integer          not null
#  tag_id     :integer          not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_venue_tags_on_tag_id    (tag_id)
#  index_venue_tags_on_venue_id  (venue_id)
#
# Foreign Keys
#
#  fk_rails_01c6ae9283  (venue_id => venues.id)
#  fk_rails_eccdfd451a  (tag_id => tags.id)
#

class VenueTag < ActiveRecord::Base
  belongs_to :venue
  belongs_to :tag
end
