# == Schema Information
#
# Table name: tags
#
#  id         :integer          not null, primary key
#  name       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#
# Indexes
#
#  index_tags_on_name  (name)
#

class Tag < ActiveRecord::Base
	VENUE_TYPES = [ 'public', 'commercial', 'residential']
	VENUE_FEATURES = [
		'wheelchair_accessible',
		'restrooms',
		'catering',
		'kitchen_dining',
		'performances',
		'security',
		'licensed_bar',
		'onsite_cleaning'
	]

	has_many :venue_tags
	has_many :venues, through: :venue_tags

	validates :name, presence: true
	validate :name_is_type_or_feature

	private

		def name_is_type_or_feature
			if !VENUE_TYPES.include?(name) && !VENUE_FEATURES.include?(name)
				errors.add(:name, "has not been declared in Tag constants") 
			end
		end

end
