# == Schema Information
#
# Table name: invites
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  token      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  event_id   :integer          not null
#  sent_at    :datetime
#
# Indexes
#
#  index_invites_on_event_id  (event_id)
#  index_invites_on_token     (token)
#
# Foreign Keys
#
#  fk_rails_30732c6b62  (event_id => events.id)
#

require 'csv'
require 'valid_email/validate_email'

class Invite < ApplicationRecord

	belongs_to :event
	has_one :attendance

	validates :email, :event_id, presence: true

	before_create :generate_token

	def self.upload(file, event_id)
		if file.content_type == "text/csv" then
			event_id = event_id
			parse_csv(file.read, event_id) 
		else
			# return error message
		end
	end

	private

		def generate_token
		  self.token = Digest::SHA1.hexdigest([Time.now, rand].join)
		end

		def self.parse_csv(file, event_id)
			csv = CSV.new(file) # uses ruby's CSV class
			csv_array = csv.to_a
			first_row = csv_array[0]
			has_header = false
			# Test if first row contains any all-numeric strings (header names cannot be numbers)
			if first_row.any? { |e| /\A[\d]*\z/ =~ e } then 
				has_header = false
				puts "CSV with no header or header with numeric fields (invalid)"
			# Test if first row contains only word-characters
			elsif first_row.all? { |e| /\A[-\w.]*\z/ =~ e } then
				has_header = true 
				puts "CSV with header and only word fields"
			end
			
			has_email = false
			email_field = nil
			email_index = nil

			if has_header then
				first_row.each do |s|
					if s.downcase.include?('email') then
						has_email = true
						email_field = s
						generate_inserts(csv_array, email_field, email_index, event_id)
						puts "Header has email-like field: " + email_field
						break
					end
				end
			# No header so we check for email regex
			else
				email_index = first_row.index { |e| /@/ =~ e }
				if email_index then 
					has_email = true
					generate_inserts(csv_array, email_field, email_index, event_id)
					puts "No header but first row contains email-like entry " + email_index.to_s
				else 
					puts "No header and no email-like entries"
				end
			end
		end
		
		def self.generate_inserts(csv_arr, email_field = nil, email_index = nil, event_id)
			total_rows = 0
			absent_email = 0
			invalid_email = 0

			inserts = []
			start_index = 0
			i = 0

			i_event_id = event_id
			i_created_at = Time.now 

			if email_field then
				i = csv_arr[0].index(email_field)
				start_index = 1
			else
				i = email_index
			end

			# Ignore first row if there is a header
			csv_arr.drop(start_index).each do |row|
				total_rows + 1
				i_email = row[i]
				if i_email.blank? then absent_email += 1
				elsif !ValidateEmail.valid?(i_email) then
					invalid_email += 1
				else
					i_token = Digest::SHA1.hexdigest([Time.now, rand].join)
					# Field `created_at` appears 2x to account for `updated_at` on creation
					inserts.push "('#{i_email}', '#{i_token}', '#{i_created_at}', '#{i_created_at}', #{i_event_id})"
				end
			end

			# puts inserts
			sql_mass_insert(inserts)

		end

		def self.sql_mass_insert(array)
			sql = "INSERT INTO invites (email, token, created_at, updated_at, event_id) VALUES #{array.join(", ")}"
			ActiveRecord::Base.connection.execute(sql)
		end

end
