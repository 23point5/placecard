# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  is_profile         :boolean          default(FALSE)
#  height             :integer
#  width              :integer
#  pictureable_type   :string
#  pictureable_id     :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  owner_id           :integer          not null
#
# Indexes
#
#  index_photos_on_owner_id                             (owner_id)
#  index_photos_on_pictureable_type_and_pictureable_id  (pictureable_type,pictureable_id)
#
# Foreign Keys
#
#  fk_rails_3ad69d141b  (owner_id => users.id)
#

class Photo < ApplicationRecord
  mount_uploader :image_file_name, PhotoUploader
  
  has_one :post
  belongs_to :owner, class_name: "User"
  belongs_to :pictureable, polymorphic: true
  
  validates :image_file_name, presence: true
  validates :owner_id, presence: true
  validates_integrity_of :image_file_name

  before_save :update_image_attributes

  def to_jq_upload
    {
      "name": read_attribute(:image_file_name),
      "size": image_file_name.size,
      "url": image_file_name.url,
      "delete_url": photo_path(id: id),
      "delete_type": "DELETE"
    }
  end

  private

    def update_image_attributes
      if image_file_name.present?
        self.image_content_type = image_file_name.content_type
        self.image_file_size = image_file_name.size
      end
    end

end
