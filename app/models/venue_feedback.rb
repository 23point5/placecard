# == Schema Information
#
# Table name: venue_feedbacks
#
#  id            :integer          not null, primary key
#  rating        :decimal(4, 2)
#  comment       :text
#  attendance_id :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_venue_feedbacks_on_attendance_id  (attendance_id)
#
# Foreign Keys
#
#  fk_rails_bc43407481  (attendance_id => attendances.id)
#

class VenueFeedback < ApplicationRecord
	include Notifiable

	belongs_to :attendance
	has_one :event, through: :attendance
	has_one :user, through: :attendance
	has_one :venue, through: :event	

	validates :attendance_id, presence: true
	validate :must_be_complete, on: :update

	after_update_commit { notify(venue.user, "new_venue_feedback", self) }

	def complete?
		rating.present? && comment.present?
	end

	private

		def must_be_complete
			unless complete?
				errors.add(:rating, "rating and comment cannot be blank")
			end
		end
end
