# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  email           :string           not null
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  username        :string
#  remember_digest :string
#
# Indexes
#
#  index_users_on_username  (username) UNIQUE
#

class User < ApplicationRecord
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	
	attr_accessor :remember_token
	has_secure_password

	has_many :events
	has_many :venues
	has_many :attendances
	has_many :posts, dependent: :destroy
	has_many :venue_feedbacks, through: :attendances
	has_many :event_feedbacks, through: :attendances
	has_many :photos, foreign_key: "owner_id"
	has_many :notifications
	has_one :photo, as: :pictureable
	has_one :profile

	accepts_nested_attributes_for :photo

	validates :first_name, presence: true
	validates :username, presence: true, uniqueness: true
	validates :email, presence: true, length: { maximum: 255 }, format: { with: VALID_EMAIL_REGEX }, 
										uniqueness: { case_sensitive: false }
	validates :password, presence: true, length: { minimum: 6 }, on: :create

	before_save { self.email.downcase! }
	after_create_commit { self.create_profile }

	def to_param
		username
	end

	def full_name
    if last_name.blank?
      first_name
    else
      "#{first_name} #{last_name}"
    end
  end

  def profile_picture
  	if @photo = self.photo
  		@photo.image_file_name_url :thumb
  	else
      "heart.png"
    end
  end

	def User.new_remember_token
    SecureRandom.urlsafe_base64
  end

  def User.digest(token)
    Digest::SHA1.hexdigest(token.to_s)
  end

  def remember
    self.remember_token = User.new_remember_token
    update_attribute(:remember_digest, User.digest(remember_token))
  end

  def authenticated?(remember_token)
  	return false if remember_digest.nil?
    BCrypt::Password.new(remember_digest).is_password?(remember_token)
  end

	def forget
    update_attribute(:remember_digest, nil)
  end

  def unread_notifications?
  	self.notifications.where(is_viewed: false).any?
  end

end
