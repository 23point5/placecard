# @WARNING - turbolinks is not cooperating with jQuery, Mapbox, and gCharts in Rails 5.0.0.beta3
# We loop through all inline script tags here, then create new script elements and attach them to DOM
$(document).on 'turbolinks:load', ->
	if $("body script")
		$("body script").each ->
			console.log(this.innerHTML)
			script = document.createElement('script')
			script.type= 'text/javascript'
			script.innerText = this.innerHTML
			$("body").append(script)
			this.remove()
