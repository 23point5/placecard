# Action Cable provides the framework to deal with WebSockets in Rails.
#
#
#= require action_cable
#= require_self
#= require_tree ./channels

@App ||= {}
App.cable = ActionCable.createConsumer()
