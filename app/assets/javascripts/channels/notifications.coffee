App.notifications = App.cable.subscriptions.create "NotificationsChannel",
  connected: ->
   	console.log "A connection has been established to notification channel."

  disconnected: ->
    console.log "The connection was disconnected from notification channel."

  received: (data) ->
  	# @TODO - add or remove active state to button
  	console.log data
  	$('#notifications').prepend data.notification
    # new Notification data["type"], content: data["content"]

  read: (notification_id) ->
  	@perform 'read', id: notification_id

  viewAll: ->
  	@perform 'view_all'

  speak: (message) ->
  	@perform 'speak', message: message
