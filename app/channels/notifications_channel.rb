class NotificationsChannel < ApplicationCable::Channel
  def subscribed
		stream_from "notify_#{current_user.id}"
  end

  def read(data)
  	notification = Notification.find(data['id'])
  	notification.read
  	notification.save
  end

  def view_all
  	current_user.notifications.update_all(is_viewed: true)
  end
end
