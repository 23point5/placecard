# Be sure to restart your server when you modify this file. Action Cable runs in a loop that does not support auto reloading.
class EventsChannel < ApplicationCable::Channel
  def subscribed
    # stream_from "event_#{params[:event_id]}"
  end

  def unsubscribed
    # stop_all_streams
  end

  # def receive(data)
  # 	ActionCable.server.broadcast "event_#{params[:event_id]}", data.post
  # end

end
