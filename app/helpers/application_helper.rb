module ApplicationHelper

	def absolute_url(path)
    "https://#{ENV['HOST']}#{path}"
  end

  def set_page_title(title)
    content_for(:page_title) do
      "#{title} | PlaceCard"
    end
  end

  def remote_ip
    request.remote_ip
  end

end
