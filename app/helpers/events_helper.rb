# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  title       :string           not null
#  description :text
#  is_public   :boolean          default(FALSE)
#  start_time  :datetime
#  end_time    :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  venue_id    :integer
#
# Indexes
#
#  index_events_on_user_id   (user_id)
#  index_events_on_venue_id  (venue_id)
#
# Foreign Keys
#
#  fk_rails_0cb5590091  (user_id => users.id)
#  fk_rails_f476266cf4  (venue_id => venues.id)
#

module EventsHelper
	
	def guest_attendance
		@attendance ||= @event.attendances.find_by_user_id(current_user.id)
	end

	def already_attending?
		!guest_attendance.nil?
	end

	def valid_token?(invite)
		# make sure the invitation has not already been used by another user
		@event.attendances.find_by_invite_id(invite.id).nil?
	end

end
