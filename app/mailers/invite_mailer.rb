class InviteMailer < ApplicationMailer

	def invitation(invite)
		@invite = invite
		@event = invite.event
		@user = @event.user
		@link = event_url(@event, token: @invite.token)

    mail(to: @invite.email,
    		 from: @user.email, 
    		 subject: "#{@user.full_name} has invited you to their event")
    invite.update_attribute(:sent_at, Time.now)
  end

end
