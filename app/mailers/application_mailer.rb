class ApplicationMailer < ActionMailer::Base
  default from: "support@placecard.com"
  default host: "#{ENV['HOST']}"
  layout 'mailer'
end
