class FeedbackGeneratorJob < ApplicationJob
  include SuckerPunch::Job

  def perform(event)
    ActiveRecord::Base.connection_pool.with_connection do
      if !event.event_feedbacks.any?
      	event.generate_feedback_forms
      end
    end
  end

end
