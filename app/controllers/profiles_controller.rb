# == Schema Information
#
# Table name: profiles
#
#  id                :integer          not null, primary key
#  is_complete       :boolean          default(FALSE)
#  gender            :string
#  age               :integer
#  ethnicity         :string
#  country_of_origin :string
#  education         :string
#  purpose           :string
#  astrology         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  user_id           :integer          not null
#
# Indexes
#
#  index_profiles_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_e424190865  (user_id => users.id)
#

class ProfilesController < ApplicationController

	def edit
		@user = current_user
		@profile = @user.profile || @user.create_profile
	end

	def update
		@user = User.find_by_username(params[:user_username])
		@profile = @user.profile
		if @profile.update_attributes(profile_params)
		  flash[:success] = "Profile updated, thanks!"
      redirect_to @user
		else
			flash.now[:info] = "Certain fields are required."
		  render 'edit'
		end
	end

	private

		def profile_params
			params.require(:profile).permit(:gender, :age, :ethnicity, :country_of_origin, :education, :purpose, :astrology)
		end
	
end
