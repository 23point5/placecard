# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  email           :string           not null
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  username        :string
#  remember_digest :string
#
# Indexes
#
#  index_users_on_username  (username) UNIQUE
#

class UsersController < ApplicationController
	before_action :logged_in_user, :correct_user, except: [:index, :new, :create, :rsvp]

	def show
		@user = User.find_by_username(params[:username])
		@upcoming_atts , @past_atts = [], []
		@user.attendances.each do |att|
			@upcoming_atts << att if att.event.end_time > DateTime.now
			@past_atts << att if (att.rsvp == 'attending' && att.event.end_time < DateTime.now)
		end
	end

	def index
		@users = User.all
		render json: @users
	end

	def new
		@user = User.new
	end

	def create
		@user = User.new(user_params)
		if @user.save
			log_in @user
			flash[:success] = "Thanks for joining PlaceCard!"
			redirect_back_or @user
		else
			render 'new'
		end
	end

	def edit
		@user = User.find_by_username(params[:username])
	end

	def update
		@user = User.find_by_username(params[:username])
		@old_photo = @user.photo
		if !photo_params[:photo].blank? 
			if @user.photos.create!(image_file_name: photo_params[:photo], pictureable: @user, is_profile: true)
				@old_photo.destroy! if @old_photo
			end
		end
		if @user.update_attributes(user_params)
		  flash[:success] = "Data saved."
      redirect_to @user
		else
			flash.now[:danger] = "Certain fields are required."
		  render 'edit'
		end
  end

  def destroy
    User.find_by_username(params[:username]).destroy
    flash[:success] = "User deleted"
    redirect_to users_url
  end

  def rsvp
  	@attendance = Attendance.find(params[:id])
  	if Attendance::RSVP.include?(rsvp_params[:rsvp]) && @attendance.update_attributes(rsvp_params)
  		redirect_back_or current_user
  	end
  end

	private

		def user_params
			params.require(:user).permit(:first_name, :last_name, :username, :email, :password, :password_confirmation)
		end

		def photo_params
			params.require(:photo)
		end

		def rsvp_params
			params.permit(:rsvp)
		end

		def logged_in_user
      unless logged_in?
        store_location
        flash[:danger] = "Please log in."
        redirect_to login_url
      end
    end

    def correct_user
      @user = User.find_by_username(params[:username])
      redirect_to(root_url) unless current_user?(@user)
    end
	
end
