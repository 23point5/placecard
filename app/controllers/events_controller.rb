# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  title       :string           not null
#  description :text
#  is_public   :boolean          default(FALSE)
#  start_time  :datetime
#  end_time    :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  venue_id    :integer
#
# Indexes
#
#  index_events_on_user_id   (user_id)
#  index_events_on_venue_id  (venue_id)
#
# Foreign Keys
#
#  fk_rails_0cb5590091  (user_id => users.id)
#  fk_rails_f476266cf4  (venue_id => venues.id)
#

class EventsController < ApplicationController
	include EventsHelper
	before_action :authenticate, except: [:public_index]
	before_filter :parse_date_params, only: [:create, :update]

	STEPS = ['basics', 'guests', 'location']

	def public_index
		@events = Event.where(is_public: true)
		@user = current_user
	end

	def index
		@user = current_user
		@events = current_user.events
	end

	# Shows the event page if the user is authorized to see it
	def show
		@event = Event.find(params[:id])
		# skip authentication if event is public
		unless @event.is_public
			# skip verification if user is the event organizer
			if !current_user?(@event.user)
				# check if the current_user has an attendance for this event
				unless already_attending?
					if params[:token] != nil
						verify_invitation
					else
						# lie to the user if they're trying to access an event they shouldn't
						flash[:danger] = "Whoops, we couldn't find that event."
					end
				end
			end
			@attendance = guest_attendance
		end
		@post = Post.new
	end

	def new
		@user = current_user
		@event = Event.new
		@event.venue_id = params[:venue_id] if params[:venue_id]
	end

	def create
		@event = current_user.events.build(event_params)
		if @event.save
			flash[:success] = "Your event has been created!"
			redirect_to user_event_manage_path(current_user, @event, "guests")
		else
			flash[:danger] = "Oops, an error occured. Please try again."
			render 'new'
		end
	end

	def manage
		@user = current_user
		@event = Event.find(params[:event_id])
		if valid_step.present?
      @step = valid_step
      @next_step = next_step
    else
      @step = 'basics'
    end
	end

	# Shows user demographics for the event
	def stats
		@user = current_user
		@event = Event.find(params[:id])
	end

	def update
		@event = Event.find(params[:id])
		if @event.update_attributes(event_params)
			if next_step 
		  	redirect_to user_event_manage_path(current_user, @event, params[:step])
		  	flash[:success] = "Your event was updated successfully."
		  else
		  	redirect_to user_event_path(current_user, @event)
		  	flash[:success] = "Awesome, your event is ready."
		  end
		else
		  flash[:danger] = "Some fields are invalid, please check your dates!"
		  redirect_back(fallback_location: :manage)
		end
	end

	def add_guests
		@event = Event.find(params[:event_id])
		if params[:invites]
			emails = params[:invites][:email]
			emails.split(', ').reject(&:empty?).each do |email|
				@invite = @event.invites.create!(email: email)
			end
		end
		if params[:event] && invite_params[:csv_file]
			if Invite.upload(invite_params[:csv_file], @event.id)
			end
		end
		flash[:success] = "Your invitations are now pending."
		redirect_back(fallback_location: :manage)
	end

	def remove_guest
		@invite =Invite.find(params[:id])
		if @invite.destroy
			flash[:success] = "Your invite was deleted."
		else
			flash.now[:danger] = "An error occurred, please try again."
		end
		redirect_back(fallback_location: user_event_manage_path(current_user, params[:event_id], "guests"))
	end

  def send_invites
		@event = Event.find(params[:event_id])
		Invite.where(event_id: @event.id).where(sent_at: nil).each do |invite|
			InviteMailer.invitation(invite).deliver_now
		end
		flash[:success] = "Your invitations have been sent."
		redirect_back(fallback_location: user_event_manage_path(current_user, @event, "guests"))
	end

	def destroy
		@event = Event.find(params[:id])
		if @event.destroy
			redirect_back(fallback_location: user_events_path)
			flash[:success] = "Your event was canceled."
		else
			flash.now[:danger] = "An error occurred, please try again."
			render 'index'
		end
	end

	def preview_venue
		@venue = Venue.find(params[:id])
		@photos = @venue.photos
		render 'venues/show'
	end

	def select_venue
		@event = Event.find(params[:event_id])
		@event.venue_id = params[:id]
		if @event.save(validate: false)
			flash[:success] = "Location saved."
			redirect_to user_event_manage_path(current_user, @event, "location")
		else
			flash[:danger] = "Something went wrong"
			render @event
		end
	end

	def remove_venue
		@event = Event.find(params[:event_id])
		@event.venue_id = nil
		if @event.save(validate: false)
			flash[:success] = "You can now select a new venue"
			redirect_back(fallback_location: :manage)
		else
			flash[:danger] = "Something went wrong"
			render :manage
		end
	end

	private

		def parse_date_params
			if !params[:event][:start_time].blank? && start_s = params[:event][:start_time]
				params[:event][:start_time] = DateTime.strptime(start_s, "%m/%d/%Y %l:%M %p")
			end 
			if !params[:event][:end_time].blank? && end_s = params[:event][:end_time]
				params[:event][:end_time] = DateTime.strptime(end_s, "%m/%d/%Y %l:%M %p")
			end
		end

		def event_params
			params.require(:event).permit(:title, :start_time, :end_time, :description, :is_public, invites_attributes: [:email])
		end

		def invite_params
			params.require(:event).permit(:csv_file)
		end

		def valid_step
	    STEPS.find { |s| s == params[:step].to_s.downcase }
	  end

	  def next_step
	  	current_step_index = STEPS.index(valid_step)
    	next_step = STEPS[current_step_index+1]
	  end

	  def verify_invitation
			@invite = @event.invites.find_by_token(params[:token])
			if valid_token?(@invite)
				@event.attendances.create(user_id: current_user.id, invite_id: @invite.id)
			else
				flash[:danger] = "Woops, that invitation is not valid."
				redirect_to current_user
			end
		end

end
