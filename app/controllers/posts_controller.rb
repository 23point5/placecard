# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer          not null
#  event_id   :integer          not null
#  photo_id   :integer
#
# Indexes
#
#  index_posts_on_event_id  (event_id)
#  index_posts_on_photo_id  (photo_id)
#  index_posts_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_5b5ddfd518  (user_id => users.id)
#  fk_rails_ad60add497  (photo_id => photos.id)
#  fk_rails_bbb8b07197  (event_id => events.id)
#

class PostsController < ApplicationController

	before_action :authenticate, only: [:create, :destroy]
	before_filter :load_event

	def create
		@post = current_user.posts.build(post_params)
		@post.event = @event
		if @post.save
			if @photo = photo_params[:photo]
				@post.create_photo!(image_file_name: @photo, owner_id: current_user.id)
			end
			flash[:success] = "Thanks for posting!"
		else
			flash[:danger] = "Oops, an error occured. Please try again."
		end
		redirect_to event_path(@event)
	end

	def destroy
		@post = @event.posts.find(params[:id])
		@post.event = @event
		if (@post.user_id == current_user.id || @event.user_id == current_user.id) && @post.destroy
			flash[:success] = "Your post was deleted."
		else
			flash.now[:danger] = "An error occurred, please try again."
		end
		redirect_to event_path(@post.event)
	end

	private

		def post_params
			params.require(:post).permit(:content)
		end

		def photo_params
			params.require(:post).permit(:photo)
		end

		def load_event
			@event = Event.find(params[:event_id])
		end
		
end
