class FeedbacksController < ApplicationController
	before_action :authenticate, :correct_user

	def rate_event_and_venue
		@attendance = Attendance.find(params[:attendance_id])
		@event_feedback = @attendance.event_feedback
		@event = @event_feedback.event
		@venue_feedback = @attendance.venue_feedback
		@venue = @venue_feedback.venue
	end

	def submit
		@event_feedback = EventFeedback.find_by_attendance_id(params[:attendance_id])
		@venue_feedback = VenueFeedback.find_by_attendance_id(params[:attendance_id])
		if @event_feedback.update_attributes(event_feedback_params)
			if @venue_feedback.update_attributes(venue_feedback_params)
				flash[:success] = "Your review was saved! Thanks for your time."
				redirect_to current_user
			end
		else
			flash[:danger] = "We couldn't save your feedback. Try again?"
			render 'rate_event_and_venue'
		end
	end

	private

		def event_feedback_params
			params.require(:event_feedback).permit(:rating, :comment)
		end

		def venue_feedback_params
			params.require(:venue_feedback).permit(:rating, :comment)
		end

		def correct_user
      @attendance = Attendance.find(params[:attendance_id])
      redirect_to current_user unless current_user?(@attendance.user)
    end

end
