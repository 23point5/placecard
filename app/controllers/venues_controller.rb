# == Schema Information
#
# Table name: venues
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  website     :string
#  latitude    :float
#  longitude   :float
#  capacity    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  city        :string
#  street      :string
#  suite_no    :string
#
# Indexes
#
#  index_venues_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_ad8f6027f0  (user_id => users.id)
#

class VenuesController < ApplicationController
	before_action :authenticate, except: [:public_index, :show]

	STEPS = ['basics', 'photos', 'location', 'amenities']
	
	def public_index
		@venues = Venue.all
		if params[:search]
			@previous_search = params[:search]
			@venues = Venue.search(params[:search])
		end
		if !params[:guests].blank?
			@venues = @venues.where("capacity > ?", params[:guests])
		end
		if params[:tags]
			@venues = @venues.filter({tagged: params[:tags]})
		end
		if params[:category]
			# find the intersection of venues with appropriate category and tags
			@venues &= Venue.category(params[:category])
		end
	end

	def index
		if @user = User.find_by_username(params[:user_username])
			@venues = @user.venues
		end
	end

	def show
		@venue = Venue.find(params[:id])
		@photos = @venue.photos.all
	end

	def new
		@user = current_user
		@venue = Venue.new
		@photo = @venue.photos.build
	end

	def create
		@venue = current_user.venues.build(venue_params)
		if @venue.save
			flash[:success] = "Success!"
			redirect_to user_venue_manage_path(current_user, @venue, "basics")
		else
			flash[:danger] = "Oops, an error occurred. Please try again."
			render 'new'
		end
	end

	def manage
		@venue = Venue.find(params[:venue_id])
		@types = Tag.where(name: Tag::VENUE_TYPES )
		@user = current_user
		if valid_step.present?
      @step = valid_step
      @next_step = next_step
    else
      @step = 'basics'
    end
	end

	def stats
		@user = current_user
		@venue = Venue.find(params[:id])
	end

	def update
		@venue = Venue.find(params[:id])
		if params[:venue] && !@venue.update_attributes(venue_params)
			flash.now[:danger] = "Oops, an error occurred. Try again?"
		  render 'manage'
		end
		flash[:success] = "Your changes have been saved."
		redirect_to user_venue_manage_path(current_user, @venue.id, params[:step])
  end

  def update_photos
  	@venue = Venue.find(params[:venue_id])
		if photos = params[:venue][:photos]
			photos.each do |image|
				if @photo = @venue.photos.create!(image_file_name: image, owner_id: current_user.id)
					respond_to do |format|
						# immediately render the new photo
						format.js { render file: "photos/new_venue"}
					end
				end
			end
		end
  end

 	def remove_photo
  	@photo = Photo.find(params[:id])
		if @photo.destroy
			flash[:success] = "Your photo was successfully deleted."
			redirect_to user_venue_manage_path(current_user, params[:venue_id], "photos")
		else
			flash.now[:danger] = "An error occurred, please try again."
		end
  end

  def update_tags
  	@venue = Venue.find(params[:venue_id])
  	@venue_tags = @venue.venue_tags
  	default_types = Tag.where(name: Tag::VENUE_TYPES)
  	# The venue owner has updated the type of their venue
  	if params[:venue_type] && !@venue_tags.include?(tag_id: params[:venue_type])
  		# destroy the old tag association(s) then add the new one
  		@venue_tags.where(tag_id: default_types).destroy_all
  		@venue.venue_tags.create(tag_id: params[:venue_type])
  	end
  	default_features = Tag.where(name: Tag::VENUE_FEATURES)
  	if params[:venue_features] && params[:venue_features].any?
  		# destroy all associations to features and recreate
  		@venue_tags.where(tag_id: default_features).destroy_all
  		new_tags = Tag.where(name: params[:venue_features])
  		new_tags.each do |tag|
  			@venue.venue_tags.create(tag_id: tag.id)
  		end
  	end
  	flash[:success] = "Awesome, your venue was tagged."
  	redirect_to :back
  end

  def continue
		if next_step 
	  	redirect_to user_venue_manage_path(current_user, @venue, next_step)
	  else
	  	redirect_to user_venue_path(current_user, @venue)
	  	flash[:success] = "Awesome, your venue is complete."
	  end
  end

	def destroy
		@venue = Venue.find(params[:id])
		if @venue.destroy
			redirect_back(fallback_location: user_venues_path)
			flash[:success] = "Your venue was successfully deleted."
		else
			flash.now[:danger] = "An error occurred, please try again."
			render 'index'
		end
	end

	private

		def venue_params
			params.require(:venue).permit(:name, :description, :website, :city, :street, :suite_no, :capacity)
		end

		def photos_params
			params.require(:venue).permit(photos_attributes: [:image_file_name])
		end

		def valid_step
	    STEPS.find { |s| s == params[:step].to_s.downcase }
	  end

	  def next_step
	  	current_step_index = STEPS.index(valid_step)
    	next_step = STEPS[current_step_index+1]
	  end

end
