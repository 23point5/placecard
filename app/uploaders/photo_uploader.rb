# encoding: utf-8

class PhotoUploader < CarrierWave::Uploader::Base
  include CarrierWave::MiniMagick
  # See config/carrierwave.rb for application-wide configurations

  def store_dir
    "uploads/#{model.class.to_s.pluralize.underscore}/#{model.pictureable_type.underscore}/#{model.id}"
  end

  # Process files as they are uploaded so we're not storing massive files
  process resize_to_fit: [800, 800]
  process :store_dimensions

  version :thumb do
    process resize_to_fill: [200, 200]
  end

  if :is_venue? || :is_post?
    version :gallery do
      process resize_to_fill: [400, 300]
    end
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

  private

    def store_dimensions
      if file && model
        model.width, model.height = ::MiniMagick::Image.open(file.file)[:dimensions]
      end
    end

    def is_venue?
      model.pictureable_type == 'venue'
    end

    def is_user?
      model.pictureable_type == 'user'
    end

    def is_event?
      model.pictureable_type == 'event'
    end

    def is_post?
      model.pictureable_type == 'post'
    end

end
