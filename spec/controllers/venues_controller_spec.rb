# == Schema Information
#
# Table name: venues
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  website     :string
#  latitude    :float
#  longitude   :float
#  capacity    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  city        :string
#  street      :string
#  suite_no    :string
#
# Indexes
#
#  index_venues_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_ad8f6027f0  (user_id => users.id)
#

require 'rails_helper'

RSpec.describe VenuesController, type: :controller do

end
