# == Schema Information
#
# Table name: posts
#
#  id         :integer          not null, primary key
#  content    :text
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  user_id    :integer          not null
#  event_id   :integer          not null
#  photo_id   :integer
#
# Indexes
#
#  index_posts_on_event_id  (event_id)
#  index_posts_on_photo_id  (photo_id)
#  index_posts_on_user_id   (user_id)
#
# Foreign Keys
#
#  fk_rails_5b5ddfd518  (user_id => users.id)
#  fk_rails_ad60add497  (photo_id => photos.id)
#  fk_rails_bbb8b07197  (event_id => events.id)
#

require 'rails_helper'

RSpec.describe PostsController, type: :controller do

  # describe "GET #create" do
  #   it "returns http success" do
  #     get :create
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "GET #edit" do
  #   it "returns http success" do
  #     get :edit
  #     expect(response).to have_http_status(:success)
  #   end
  # end

  # describe "GET #destroy" do
  #   it "returns http success" do
  #     get :delete
  #     expect(response).to have_http_status(:success)
  #   end
  # end

end
