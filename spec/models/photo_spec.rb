require 'rails_helper'

RSpec.describe Photo, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:photo) { FactoryGirl.build_stubbed(:photo) }

	it 'is valid with valid attributes' do
    expect(photo).to be_valid
  end

  # can't test for valid image_file_name until after saving (Carrierwave)
  # validates :image_file_name, presence: true

  describe '#owner_id' do
    it 'is required' do
      photo.owner_id = nil
      photo.valid?
      expect(photo.errors[:owner_id].size).to eq(1)
    end
  end
end
