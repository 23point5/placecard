require 'rails_helper'

RSpec.describe VenueFeedback, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:venue_feedback) { FactoryGirl.build_stubbed(:venue_feedback) }

	it 'is valid with valid attributes' do
    expect(venue_feedback).to be_valid
  end

	# validates :attendance_id, presence: true
	# validate :must_be_complete, on: :update

  describe '#attendance_id' do
    it 'is required' do
      venue_feedback.attendance_id = nil
      venue_feedback.valid?
      expect(venue_feedback.errors[:attendance_id].size).to eq(1)
    end
  end
end
