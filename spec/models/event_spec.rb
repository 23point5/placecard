require 'rails_helper'

RSpec.describe Event, type: :model do
	# try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	# set the start_time to pass future_start_time validation
	let(:event) { FactoryGirl.build_stubbed(:event, start_time: Time.now + 1.day) }

	it 'is valid with valid attributes' do
    expect(event).to be_valid
  end

  # validates :title, :description, :user_id, presence: true
	# validate :duration_must_be_positive
	# validate :future_start_time, on: :create
	# validate :future_start_time, on: :update, if: "self.start_time_changed?"
	
	describe '#title' do
    it 'is required' do
      event.title = nil
      event.valid?
      expect(event.errors[:title].size).to eq(1)
    end
  end

  describe '#description' do
    it 'is required' do
      event.description = nil
      event.valid?
      expect(event.errors[:description].size).to eq(1)
    end
  end

  describe '#user_id' do
    it 'is required' do
      event.user_id = nil
      event.valid?
      expect(event.errors[:user_id].size).to eq(1)
    end
  end

end
