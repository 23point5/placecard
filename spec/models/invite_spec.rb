require 'rails_helper'

RSpec.describe Invite, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:invite) { FactoryGirl.build_stubbed(:invite) }

	it 'is valid with valid attributes' do
    expect(invite).to be_valid
  end

  # validates :email, :event_id, presence: true
	
	describe '#email' do
    it 'is required' do
      invite.email = nil
      invite.valid?
      expect(invite.errors[:email].size).to eq(1)
    end
  end

  describe '#event_id' do
    it 'is required' do
      invite.event_id = nil
      invite.valid?
      expect(invite.errors[:event_id].size).to eq(1)
    end
  end
end
