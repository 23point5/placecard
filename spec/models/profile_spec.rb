require 'rails_helper'

RSpec.describe Profile, :type => :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:profile) { FactoryGirl.build_stubbed(:profile) }

	it 'is valid with valid attributes' do
    expect(profile).to be_valid
  end
  
  describe '#user_id' do
    it 'is required' do
      profile.user_id = nil
      profile.valid?
      expect(profile.errors[:user_id].size).to eq(1)
    end
  end
end
