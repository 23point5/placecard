require 'rails_helper'

RSpec.describe Tag, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:tag) { FactoryGirl.build_stubbed(:tag) }

	it 'is valid with valid attributes' do
    expect(tag).to be_valid
  end
  
  describe '#name' do
    it 'is required' do
      tag.name = nil
      tag.valid?
      # name should be invalid for presence and existance in Tag constants
      expect(tag.errors[:name].size).to be > 1
    end
  end
end
