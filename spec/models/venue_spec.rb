require 'rails_helper'

RSpec.describe Venue, type: :model do
	# try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:venue) { FactoryGirl.build_stubbed(:venue) }

	it 'is valid with valid attributes' do
    expect(venue).to be_valid
  end

  # validates :user_id, :name, :city, :street, :capacity, presence: true
	# validates :address, presence: true
  
  describe '#name' do
    it 'is required' do
      venue.name = nil
      venue.valid?
      expect(venue.errors[:name].size).to eq(1)
    end
  end

  describe '#city' do
    it 'is required' do
      venue.city = nil
      venue.valid?
      expect(venue.errors[:city].size).to eq(1)
    end
  end

  describe '#street' do
    it 'is required' do
      venue.street = nil
      venue.valid?
      expect(venue.errors[:street].size).to eq(1)
    end
  end

  describe '#capacity' do
    it 'is required' do
      venue.capacity = nil
      venue.valid?
      expect(venue.errors[:capacity].size).to eq(1)
    end
  end

  describe '#user_id' do
    it 'is required' do
      venue.user_id = nil
      venue.valid?
      expect(venue.errors[:user_id].size).to eq(1)
    end
  end
  
end
