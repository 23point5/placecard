require 'rails_helper'

RSpec.describe Post, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:post) { FactoryGirl.build_stubbed(:post) }

	it 'is valid with valid attributes' do
    expect(post).to be_valid
  end

	# validates :user_id, presence: true
	# validates :event_id, presence: true
	# validates :content, presence: true, length: { maximum: 500 }

  describe '#user_id' do
    it 'is required' do
      post.user_id = nil
      post.valid?
      expect(post.errors[:user_id].size).to eq(1)
    end
  end

  describe '#event_id' do
    it 'is required' do
      post.event_id = nil
      post.valid?
      expect(post.errors[:event_id].size).to eq(1)
    end
  end

  describe '#content' do
    it 'is required' do
      post.content = nil
      post.valid?
      expect(post.errors[:content].size).to eq(1)
    end
  end
end
