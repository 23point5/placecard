require 'rails_helper'

RSpec.describe EventFeedback, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:event_feedback) { FactoryGirl.build_stubbed(:event_feedback) }

	it 'is valid with valid attributes' do
    expect(event_feedback).to be_valid
  end

	# validates :attendance_id, presence: true
	# validate :must_be_complete, on: :update

  describe '#attendance_id' do
    it 'is required' do
      event_feedback.attendance_id = nil
      event_feedback.valid?
      expect(event_feedback.errors[:attendance_id].size).to eq(1)
    end
  end
end
