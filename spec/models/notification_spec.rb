require 'rails_helper'

RSpec.describe Notification, :type => :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:notification) { FactoryGirl.build_stubbed(:notification) }

	it 'is valid with valid attributes' do
    expect(notification).to be_valid
  end

  describe '#user_id' do
    it 'is required' do
      notification.user_id = nil
      notification.valid?
      expect(notification.errors[:user_id].size).to eq(1)
    end
  end
end
