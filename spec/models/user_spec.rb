require 'rails_helper'

RSpec.describe User, type: :model do
	# try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:user) { FactoryGirl.build_stubbed(:user) }

	it 'is valid with valid attributes' do
    expect(user).to be_valid
  end

  describe '#first_name' do
    it 'is required' do
      user.first_name = nil
      user.valid?
      expect(user.errors[:first_name].size).to eq(1)
    end
  end

  describe '#username' do
    it 'is required' do
      user.username = nil
      user.valid?
      expect(user.errors[:username].size).to eq(1)
    end

    it 'is unique' do
    	# save another_user to generate errors on user
    	another_user = FactoryGirl.create(:user, username: user.username)
    	user.valid?
      expect(user.errors[:username].size).to eq(1)
    end
  end

  describe '#email' do
    it 'is required' do
      user.email = nil
      user.valid?
      # invalid format and nil presence == 2 errors
      expect(user.errors[:email].size).to be > 1
    end

    it 'is unique' do
    	# save another_user to generate errors on user
    	another_user = FactoryGirl.create(:user, email: user.email)
    	user.valid?
      expect(user.errors[:email].size).to eq(1)
    end

    it 'is formatted' do
    	user.email = '.anything wrong?'
    	user.valid?
    	expect(user.errors[:email].size).to eq(1)
    end
  end

  describe '#password' do
    it 'is required' do
      user.password = nil
      user.valid?
      expect(user.errors[:password].size).to eq(1)
    end
  end

  describe '#full_name' do
    it 'combines first_ and last_ name' do
      user.first_name = "John"
      user.last_name = "Smith"
      expect(user.full_name).to eq("John Smith")
    end
  end

end
