require 'rails_helper'

RSpec.describe Attendance, type: :model do
  # try to use `build_stubbed` when possible so we don't hit the DB (it's faster)
	let(:attendance) { FactoryGirl.build_stubbed(:attendance) }

	it 'is valid with valid attributes' do
    expect(attendance).to be_valid
  end

  # validates :user_id, :event_id, :invite_id, presence: true
	
	describe '#user_id' do
    it 'is required' do
      attendance.user_id = nil
      attendance.valid?
      expect(attendance.errors[:user_id].size).to eq(1)
    end
  end

  describe '#event_id' do
    it 'is required' do
      attendance.event_id = nil
      attendance.valid?
      expect(attendance.errors[:event_id].size).to eq(1)
    end
  end

  describe '#invite_id' do
    it 'is required' do
      attendance.invite_id = nil
      attendance.valid?
      expect(attendance.errors[:invite_id].size).to eq(1)
    end
  end
end
