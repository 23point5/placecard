# == Schema Information
#
# Table name: profiles
#
#  id                :integer          not null, primary key
#  is_complete       :boolean          default(FALSE)
#  gender            :string
#  age               :integer
#  ethnicity         :string
#  country_of_origin :string
#  education         :string
#  purpose           :string
#  astrology         :string
#  created_at        :datetime         not null
#  updated_at        :datetime         not null
#  user_id           :integer          not null
#
# Indexes
#
#  index_profiles_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_e424190865  (user_id => users.id)
#

FactoryGirl.define do
  factory :profile do
    user_id { 
    	(User.all.count > 0 ? User.offset(rand(User.count)).first.id : create(:user).id)
    }
  end
end
