# == Schema Information
#
# Table name: attendances
#
#  id         :integer          not null, primary key
#  rsvp       :string
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  event_id   :integer          not null
#  user_id    :integer          not null
#  invite_id  :integer          not null
#
# Indexes
#
#  index_attendances_on_event_id   (event_id)
#  index_attendances_on_invite_id  (invite_id)
#  index_attendances_on_user_id    (user_id)
#
# Foreign Keys
#
#  fk_rails_777eb7170a  (event_id => events.id)
#  fk_rails_77ad02f5c5  (user_id => users.id)
#  fk_rails_d50c017781  (invite_id => invites.id)
#

FactoryGirl.define do
  factory :attendance do
  	event_id {
  		(Event.all.count > 0 ? Event.offset(rand(Event.count)).first.id : create(:event).id)
  	}
  	user_id {
  		(User.all.count > 0 ? User.offset(rand(User.count)).first.id : create(:user).id)
  	}
  	invite_id {
  		(Invite.all.count > 0 ? Invite.offset(rand(Invite.count)).first.id : create(:invite).id)
  	}
  end

end
