# == Schema Information
#
# Table name: event_feedbacks
#
#  id            :integer          not null, primary key
#  rating        :decimal(4, 2)
#  comment       :text
#  attendance_id :integer          not null
#  created_at    :datetime         not null
#  updated_at    :datetime         not null
#
# Indexes
#
#  index_event_feedbacks_on_attendance_id  (attendance_id)
#
# Foreign Keys
#
#  fk_rails_87f91bf6f6  (attendance_id => attendances.id)
#

FactoryGirl.define do
  factory :event_feedback do
    rating { rand(6) }
    comment { Faker::Lorem.sentence }
    attendance_id {
      (Attendance.all.count > 0 ? Attendance.offset(rand(Attendance.count)).first.id : create(:attendance).id)
    }
  end
end
