# == Schema Information
#
# Table name: invites
#
#  id         :integer          not null, primary key
#  email      :string           not null
#  token      :string           not null
#  created_at :datetime         not null
#  updated_at :datetime         not null
#  event_id   :integer          not null
#  sent_at    :datetime
#
# Indexes
#
#  index_invites_on_event_id  (event_id)
#  index_invites_on_token     (token)
#
# Foreign Keys
#
#  fk_rails_30732c6b62  (event_id => events.id)
#

FactoryGirl.define do
  factory :invite do
    email {
    	if rand(2) > 0 then
    		(User.all.count > 0 ? User.offset(rand(User.count)).first.email : create(:user).email)
    	else
    		Faker::Internet.free_email
    	end
    }
    token { SecureRandom.urlsafe_base64(10, false) }
    event_id {
    	(Event.all.count > 0 ? Event.offset(rand(Event.count)).first.id : create(:event).id)
    }
  end

end
