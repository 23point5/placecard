# == Schema Information
#
# Table name: photos
#
#  id                 :integer          not null, primary key
#  image_file_name    :string
#  image_content_type :string
#  image_file_size    :integer
#  is_profile         :boolean          default(FALSE)
#  height             :integer
#  width              :integer
#  pictureable_type   :string
#  pictureable_id     :integer
#  created_at         :datetime         not null
#  updated_at         :datetime         not null
#  owner_id           :integer          not null
#
# Indexes
#
#  index_photos_on_owner_id                             (owner_id)
#  index_photos_on_pictureable_type_and_pictureable_id  (pictureable_type,pictureable_id)
#
# Foreign Keys
#
#  fk_rails_3ad69d141b  (owner_id => users.id)
#

FactoryGirl.define do
  factory :photo do
  	image_file_name { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app', 'assets', 'images', 'icon.png')) }
  	image_content_type "jpg"
    image_file_size { 1000 + rand(400) }
    height { rand(900) }
    width { rand(900) }
    owner_id {
    	(User.all.count > 0 ? User.offset(rand(User.count)).first.id : create(:user).id)
    }
    pictureable_id :owner_id
    pictureable_type 'User'
  end

end
