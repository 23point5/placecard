# == Schema Information
#
# Table name: users
#
#  id              :integer          not null, primary key
#  first_name      :string
#  last_name       :string
#  email           :string           not null
#  password_digest :string
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#  username        :string
#  remember_digest :string
#
# Indexes
#
#  index_users_on_username  (username) UNIQUE
#

FactoryGirl.define do
  factory :user do
    first_name { Faker::Name.first_name }
    last_name { Faker::Name.last_name }
    username { ("#{first_name}#{last_name}").downcase }
    email { Faker::Internet.free_email("#{first_name}_#{last_name}") }
    password { Faker::Internet.password(10, 20) }
  end

end
