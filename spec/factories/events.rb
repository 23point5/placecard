# == Schema Information
#
# Table name: events
#
#  id          :integer          not null, primary key
#  title       :string           not null
#  description :text
#  is_public   :boolean          default(FALSE)
#  start_time  :datetime
#  end_time    :datetime
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  venue_id    :integer
#
# Indexes
#
#  index_events_on_user_id   (user_id)
#  index_events_on_venue_id  (venue_id)
#
# Foreign Keys
#
#  fk_rails_0cb5590091  (user_id => users.id)
#  fk_rails_f476266cf4  (venue_id => venues.id)
#

FactoryGirl.define do
  factory :event do
    to_create { |instance| instance.save(validate: false, ) }

    title { Faker::Company.buzzword.capitalize + " " + Faker::App.name }
    description { Faker::Lorem.sentence }
    is_public { rand(2) > 0 ? true : false }
    start_time { Faker::Date.between(10.days.ago, Date.today + 10) }
    end_time { "#{start_time}".to_datetime + rand(10).hours }
    user_id {
    	(User.all.count > 0 ? User.offset(rand(User.count)).first.id : create(:user).id)
    }
    venue_id {
    	(Venue.all.count > 0 ? Venue.offset(rand(Venue.count)).first.id : create(:venue).id)
    }
  end

end
