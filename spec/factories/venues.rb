# == Schema Information
#
# Table name: venues
#
#  id          :integer          not null, primary key
#  name        :string
#  description :text
#  website     :string
#  latitude    :float
#  longitude   :float
#  capacity    :integer
#  created_at  :datetime         not null
#  updated_at  :datetime         not null
#  user_id     :integer
#  city        :string
#  street      :string
#  suite_no    :string
#
# Indexes
#
#  index_venues_on_user_id  (user_id)
#
# Foreign Keys
#
#  fk_rails_ad8f6027f0  (user_id => users.id)
#

FactoryGirl.define do
  factory :venue do
    name { Faker::Company.name + " Place" }
    description { Faker::Hipster.paragraph(2) }
    website { Faker::Internet.url }
    street { Faker::Address.street_name }
    city { Faker::Address.city }
    # latitude { Faker::Address.latitude }
    # longitude { Faker::Address.longitude }
    capacity { Faker::Number.between(50, 2000) }
    user_id { 
    	(User.all.count > 0 ? User.offset(rand(User.count)).first.id : create(:user).id)
    }
  end

end
