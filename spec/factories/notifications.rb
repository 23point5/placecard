# == Schema Information
#
# Table name: notifications
#
#  id             :integer          not null, primary key
#  message        :string
#  trigger        :string
#  user_id        :integer          not null
#  alertable_type :string
#  alertable_id   :integer
#  is_viewed      :boolean          default(FALSE)
#  is_read        :boolean          default(FALSE)
#  created_at     :datetime         not null
#  updated_at     :datetime         not null
#
# Indexes
#
#  index_notifications_on_alertable_type_and_alertable_id  (alertable_type,alertable_id)
#  index_notifications_on_user_id                          (user_id)
#
# Foreign Keys
#
#  fk_rails_b080fb4855  (user_id => users.id)
#

FactoryGirl.define do
  factory :notification do
    user_id {
    	(User.all.count > 0 ? User.offset(rand(User.count)).first.id : create(:user).id)
    }
  end
end
