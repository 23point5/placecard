# PlaceCard

PlaceCard is a Ruby on Rails application built for Comp 361 by Saige, Gabi, Kathryn, and Zili. It was developed using Agile Scrum methodology between February and March 2016. Features of the app include:

  - user registration & authentication
  - public venue listings and public/private event listings
  - guest invitations, posts, and photo uploads
  - real-time notifications
  - demographics data for venues and events

PlaceCard can be run locally using the standard Rails commands (`bundle install`, `rake db:create`, `rake db:migrate`, `rails s`, etc.) - this assumes the user has the Ruby language and the bundler gem installed locally, and has started a Postgres server. The maps will not display in development as they require a Mapbox API key, which we configure in the production environment using Figaro.

If you have any questions, *please* feel free to contact a member of the PlaceCard team :)

### Contributions
Although all members of the team touched nearly all components of the app at one time or another, significant contributions to the project are listed alphabetically (by contributor last name) below.

##### Saige McVea
- high-level architecture & ENV configuration
- real-time notifications
    - updating to Rails 5.0.0.beta3 for ActionCable functionality
- CSV processing & guest invitations (including mailer)
- 'attendance' management
    - including RSVPs and event visibility permissions

##### Gabriela Stefanini
- user registration and authentication (from scratch)
- user profiles ("Tell us more") for display of demographics information
- photo uploads using Carrierwave and JQuery FileUpload
- user posts to event page
    - including CRUD operations for authorized users
- venue tagging

##### Kathryn Taylor
- CRUD operations for venue model and venue feedbacks
- venue search and venue filtering
    - using Geocoder gem to sort by proximity
    - by venue tags and/or type
- design of user dashboard
- "STEP" user-flow for creating venues (which was additionally used in events)

##### Zili Zhang
- CRUD operations for event model and event feedbacks
- venue recommendations for events based on input data
    - selection of venue for event creation (pre-entered) site-wide
    - selection of venue for existing events
- landing page design, and various other front-end elements
- complex queries to database + GoogleCharts for data visualization

We feel that all team members contributed equally to bug-fixes, documentation, and overall application development. Communication and collaboration primarily occurred on BitBucket and Slack.
